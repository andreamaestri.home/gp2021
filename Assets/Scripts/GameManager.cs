﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private Transform boundaryTop; public Transform getTopBoundary() { return boundaryTop; }
    private Transform boundaryBottom; public Transform getBottomBoundary() { return boundaryBottom; }
    private Transform boundaryLeft; public Transform getLeftBoundary() { return boundaryLeft; }
    private Transform boundaryRight; public Transform getRightBoundary() { return boundaryRight; }

    private Transform spawnPoint; public Transform getSpawnPoint() { return spawnPoint; }
    private Transform removePoint; public Transform getRemovePoint() { return removePoint; }

    private PlayerController playerController; public PlayerController getPlayerController() { return playerController; }

    private AssetManager assetManager; public AssetManager getAssetManager() { return assetManager; }

    private const float maxObstacleSpawnTime = 0.48f;
    private float obstacleSpawnTimer = 0f;

    private int score; public void addToScore(int a) { score += a; }
    private int getGameProgress() { return (score / levelSegment) - 1; }//-1 for no pets, 0 for one pet, 1 for two pets

    private PetController[] allPets;
    private int levelSegment = 20;

    private List<int> petOrder;//Ordered indexes of first to last pets to be collected - from the first the others can be updated
    private bool[] petTypeDespawned;//An array of all the pets that have despawned, the game can't end until each has spawned once 
    public void setPetTypeDespawned(int index, bool b) { petTypeDespawned[index] = b; }

    //Variables required temporarily
    private int[] spawnBufferIndex;
    private float reflecting;

    private bool shownEasterEgg = false;

    private const float maxInvulnerableTimer = 3f;
    private float invulnerableTimer = maxInvulnerableTimer;

    // Start is called before the first frame update
    void Awake()
    {
        petOrder = new List<int>();
        spawnBufferIndex = new int[6] { 0, 0, 0, 0, 0, 0 };
        petTypeDespawned = new bool[4] { false, false, false, false };

        //Set Physics Layer Collision Ignores
        Physics.IgnoreLayerCollision(GameData.PlayerInactive, GameData.Obstacle);//PlayerInactive can pass through Obstacle
        Physics.IgnoreLayerCollision(GameData.Obstacle, GameData.Boundary);//PlayerInactive can pass through Obstacle
        Physics.IgnoreLayerCollision(GameData.Obstacle, GameData.Floor);//PlayerInactive can pass through Floor

        //Retrieve pets - Will get them in the order 0:Bird, 1:Turtle, 2:Snake, 3:Dog
        allPets = new PetController[4];
        for (int i = 0; i < allPets.Length; i++)
        {
            allPets[i] = GameObject.Find("Pet" + i.ToString()).GetComponent<PetController>();
            allPets[i].setFollowPoisitionIndex(i);
            allPets[i].setupPet();
        }

        //Retrieve boundary objects for the game
        boundaryTop = GameObject.Find("TopBoundaries").transform;
        boundaryBottom = GameObject.Find("BottomBoundaries").transform;
        boundaryLeft = GameObject.Find("LeftBoundaries").transform;
        boundaryRight = GameObject.Find("RightBoundaries").transform;

        spawnPoint = GameObject.Find("SpawnPoint").transform;
        removePoint = GameObject.Find("RemovePoint").transform;

        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        assetManager = GetComponent<AssetManager>();

        score = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Spawn obstacles
        if (obstacleSpawnTimer <= 0f)
        {
            obstacleSpawnTimer = maxObstacleSpawnTime;//Reset timer
            //Find Obstacle to spawn in
            reflecting = (Random.Range(0, 2) == 0)? 1f : -1f;
            if (!shownEasterEgg && Random.Range(0, 500) == 1)
            {
                spawnObstacle(assetManager.getObstacle_eemask(), assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, (Random.Range(0, 2) == 0 ? 3f : 4f));
                shownEasterEgg = true;
            }
            else
            {
                //Set types of conkers which can spawn based on position in level
                switch (Random.Range(0, getGameProgress() < 1f ? ((Random.Range(0, 3) == 0) ? 13 : 12) : (getGameProgress() < 3f ? 14 : 15)))
                {
                    case 0:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall0s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                        }
                        break;
                    case 1:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall1s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                        }
                        break;
                    case 2:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_trunk0s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_trunk0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                        }
                        break;
                    case 3:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_trunk1s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_trunk1s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                        }
                        break;
                    case 4:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall0s())) != -1
                            && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_wall1s())) != -1
                            && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], -1, -1)) != -1
                            && (spawnBufferIndex[3] = testForFreeObstacle(assetManager.getObstacle_trunk1s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 0f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 0f);
                            spawnObstacle(assetManager.getObstacle_trunk1s()[spawnBufferIndex[3]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                        }
                        break;
                    case 5:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall0s())) != -1
                            && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_wall1s())) != -1
                            && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], -1, -1)) != -1
                            && (spawnBufferIndex[3] = testForFreeObstacle(assetManager.getObstacle_trunk0s())) != -1
                            && (spawnBufferIndex[4] = testForFreeObstacle(assetManager.getObstacle_trunk1s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 0f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 0f);
                            spawnObstacle(assetManager.getObstacle_trunk0s()[spawnBufferIndex[3]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                            spawnObstacle(assetManager.getObstacle_trunk1s()[spawnBufferIndex[4]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 1f);
                        }
                        break;
                    case 6:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall0s())) != -1
                            && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_wall1s())) != -1
                            && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], -1, -1)) != -1
                            && (spawnBufferIndex[3] = testForFreeObstacle(assetManager.getObstacle_wall1s(), spawnBufferIndex[1], -1, -1)) != -1
                            && (spawnBufferIndex[4] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], spawnBufferIndex[2], -1)) != -1
                            && (spawnBufferIndex[5] = testForFreeObstacle(assetManager.getObstacle_wall1s(), spawnBufferIndex[1], spawnBufferIndex[3], -1)) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 0f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 0f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[3]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[4]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 1f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[5]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 2f);
                        }
                        break;
                    case 7:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_wall0s())) != -1
                            && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_wall1s())) != -1
                            && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], -1, -1)) != -1
                            && (spawnBufferIndex[3] = testForFreeObstacle(assetManager.getObstacle_wall1s(), spawnBufferIndex[1], -1, -1)) != -1
                            && (spawnBufferIndex[4] = testForFreeObstacle(assetManager.getObstacle_wall0s(), spawnBufferIndex[0], spawnBufferIndex[2], -1)) != -1
                            && (spawnBufferIndex[5] = testForFreeObstacle(assetManager.getObstacle_wall1s(), spawnBufferIndex[1], spawnBufferIndex[3], -1)) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 0f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 0f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 1f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[3]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                            spawnObstacle(assetManager.getObstacle_wall0s()[spawnBufferIndex[4]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 2f);
                            spawnObstacle(assetManager.getObstacle_wall1s()[spawnBufferIndex[5]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 3f);
                        }
                        break;
                    case 8:
                    /*
                    if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_platform0s())) != -1
                        && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_platform1s())) != -1
                        && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_platform2s())) != -1)
                    {
                        spawnObstacle(assetManager.getObstacle_platform0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 2f);
                        spawnObstacle(assetManager.getObstacle_platform1s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 2f);
                        spawnObstacle(assetManager.getObstacle_platform2s()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 2f);
                    }
                    break;*/
                    case 9:
                        if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_platform0s())) != -1
                            && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_platform2s())) != -1)
                        {
                            spawnObstacle(assetManager.getObstacle_platform0s()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 1f);
                            spawnObstacle(assetManager.getObstacle_platform2s()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 1f);
                        }
                        break;
                    case 10:
                    case 11:
                        if (!assetManager.getPet_bird().activeSelf && !assetManager.getPet_turtle().activeSelf
                            && !assetManager.getPet_dog().activeSelf && !assetManager.getPet_snake().activeSelf)
                        {
                            switch (Random.Range(0, 4))
                            {
                                case 0:
                                    if (!testForPet(0))
                                        spawnObstacle(assetManager.getPet_bird(), assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, Random.Range(0, 2));
                                    break;
                                case 1:
                                    if (!testForPet(1))
                                        spawnObstacle(assetManager.getPet_turtle(), assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, Random.Range(0, 2));
                                    break;
                                case 2:
                                    if (!testForPet(2))
                                        spawnObstacle(assetManager.getPet_snake(), assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, Random.Range(0, 2));
                                    break;
                                case 3:
                                    if (!testForPet(3))
                                        spawnObstacle(assetManager.getPet_dog(), assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, Random.Range(0, 2));
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    //Conkers
                    case 12:
                    case 13:
                    case 14:
                        switch (Random.Range(0, 3))
                        {
                            case 0:
                                if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_conkers())) != -1
                                    && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_conkers(), spawnBufferIndex[0], -1, -1)) != -1
                                    && (spawnBufferIndex[2] = testForFreeObstacle(assetManager.getObstacle_conkers(), spawnBufferIndex[0], spawnBufferIndex[1], -1)) != -1)
                                //&& (spawnBufferIndex[3] = testForFreeObstacle(assetManager.getObstacle_conkers(), spawnBufferIndex[0], spawnBufferIndex[1], spawnBufferIndex[2])) != -1)
                                {
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 4f);
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 2f, 3f);
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[2]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 1.5f);
                                    //spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[3]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 1f);
                                }
                                break;
                            case 1:
                                if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_conkers())) != -1
                                    && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_conkers(), spawnBufferIndex[0], -1, -1)) != -1)
                                {
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 5f);
                                }
                                break;
                            case 2:
                                if ((spawnBufferIndex[0] = testForFreeObstacle(assetManager.getObstacle_conkers())) != -1
                                    && (spawnBufferIndex[1] = testForFreeObstacle(assetManager.getObstacle_conkers(), spawnBufferIndex[0], -1, -1)) != -1)
                                {
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[0]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x, 1f);
                                    spawnObstacle(assetManager.getObstacle_conkers()[spawnBufferIndex[1]], assetManager.getHalfDimensionsObstacle(), spawnPoint.localPosition.x + assetManager.getHalfDimensionsObstacle().x * 4f, 5f);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            obstacleSpawnTimer -= Time.deltaTime;
        }

        //Handle pets
        if (getGameProgress() >= allPets.Length + 2 && petTypeDespawned[0] && petTypeDespawned[1] && petTypeDespawned[2] && petTypeDespawned[3])
            endGame(true);
        /*else if (getPetProgress() >= 0 && !allPets[getPetProgress()].isActive())
        {

        }*/

        if (invulnerableTimer > 0f)
        {
            invulnerableTimer -= Time.deltaTime;
            if (invulnerableTimer <= 0f)
                playerController.setInvulnerablity(false);
        }

        //Update first pet and it should update the one following and so on.
        if (petOrder.Count > 0)
        {
            allPets[petOrder[0]].setTopFollowPoisition(playerController.getPlayerTop().transform.localPosition.x - GameData.petSpacing, playerController.getPlayerTop().transform.localPosition.y);
        }
    }

    public void addPet(int petIndex)
    {
        petOrder.Add(petIndex);
        if (petOrder.Count > 1)//Set this pet as the pet that the previous pet must send position updates to
        {
            allPets[petOrder[petOrder.Count - 2]].setPetFollowing(allPets[petIndex]);
        }
        allPets[petIndex].awakenPet();
    }

    private void spawnObstacle(GameObject spawningObj, Vector2 halfDimensions, float spawnPosX, float spawnLevelY)
    {
        //Add 0.001 to halfDimensions Y to stop corners from impacting the other side's player
        spawningObj.transform.localPosition = new Vector3(spawnPosX, (halfDimensions.y + (halfDimensions.y * spawnLevelY * 2f)) * reflecting, 0f);
        spawningObj.transform.localRotation = new Quaternion((reflecting > 0) ? 0f : Mathf.PI, 0f, 0f, 0f);//Quaternion.Euler((reflecting > 0)? 0f : 180f, 0f, 0f);
        spawningObj.GetComponent<Obstacle>().setupObstacle((reflecting > 0) ? getAssetManager().getColourTop() : getAssetManager().getColourMirror());
        spawningObj.SetActive(true);
    }

    //Test all of an array to see if there is a free obstalce to be used
    private int testForFreeObstacle(GameObject[] arr)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            if (!arr[i].activeSelf)
                return i;
        }
        return -1;
    }
    private int testForFreeObstacle(GameObject[] arr, int excludeIndex0, int excludeIndex1, int excludeIndex2)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            if (!arr[i].activeSelf && i != excludeIndex0 && i != excludeIndex1 && i != excludeIndex2)
                return i;
        }
        return -1;
    }

    public void updatePetMirroring(Color topCol, Color mirrorCol)
    {
        for (int i = 0; i < allPets.Length; i++)
        {
            if (allPets[i].gameObject.activeSelf)
                allPets[i].setPetColours(topCol, mirrorCol);
        }
    }

    public void endGame(bool won)
    {
        Debug.Log("End game: " + won.ToString());
        //Time.timeScale = 0f;
        GameObject MC = GameObject.Find("Main Camera");
        if (won)
        {
            MC.GetComponent<MenuController>().GameWon(testForPet((int)GameData.PetIndex.Dog), testForPet((int)GameData.PetIndex.Snake), testForPet((int)GameData.PetIndex.Bird), testForPet((int)GameData.PetIndex.Turtle));
        }
        else
        {
            MC.GetComponent<MenuController>().GameOver(testForPet((int)GameData.PetIndex.Dog), testForPet((int)GameData.PetIndex.Snake), testForPet((int)GameData.PetIndex.Bird), testForPet((int)GameData.PetIndex.Turtle));
        }
    }

    private bool testForPet(int index)
    {
        foreach (int i in petOrder)
        {
            if (i == index)
                return true;
        }
        return false;
    }

    public void hitConker()
    {
        if (invulnerableTimer <= 0f)
        {
            if (petOrder.Count > 0)
            {
                allPets[petOrder[petOrder.Count - 1]].resetPet();
                petOrder.RemoveAt(petOrder.Count - 1);
                invulnerableTimer = maxInvulnerableTimer;
                playerController.setInvulnerablity(true);
            }
            else
                endGame(false);
        }
    }
}
