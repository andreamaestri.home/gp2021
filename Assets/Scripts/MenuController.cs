﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    bool bPlay = false;
    bool bExit = false;
    bool bBack = false;
    bool bResume = false;
    bool bPaused = false;
    bool bCredits = false;
    bool bMainMenu = false;
    bool bSettings = false;
    bool bSettingsPaused = false;
    bool bHowToPlay = false;
    bool bGameOver = false;

    public GameObject settingsCanvas;
    public GameObject howToPlayCanvas;
    public GameObject CreditsCanvas;
    public GameObject mainMenu_PauseMenuCanvas;
    public GameObject GameOverCanvas;
    public GameObject GameWonCanvas;

    public Slider musicVol;
    public Slider soundFXVol;

    public AudioClip buttonClickedSound;

    public Text musicVolumePercent;
    public Text soundFXVolumePercent;

    public Image Animal1;
    public Image Animal2;
    public Image Animal3;
    public Image Animal4;

    public Image Animal1Won;
    public Image Animal2Won;
    public Image Animal3Won;
    public Image Animal4Won;

    public Sprite turtalSprite;
    public Sprite birdSprite;
    public Sprite dogSprite;
    public Sprite snakeSprite;

    SoundController SC;

    // Start is called before the first frame update
    void Start()
    {

        if (GameObject.Find("SoundController") != null)
        {
            SC = GameObject.Find("SoundController").GetComponent<SoundController>();
            string sPersent;
            musicVol.value = SC.GetComponent<SoundController>().GetOldMusicVolume();
            sPersent = (SC.GetComponent<SoundController>().GetOldMusicVolume() * 100).ToString("F1") + "%";
            musicVolumePercent.text = sPersent;
            soundFXVol.value = SC.GetComponent<SoundController>().GetOldSFXVolume();
            sPersent = (SC.GetComponent<SoundController>().GetOldSFXVolume() * 100).ToString("F1") + "%";
            soundFXVolumePercent.text = sPersent;
        }
    }
    public void SearchAllButtons()
    {
        GameObject[] buttons;
        buttons = GameObject.FindGameObjectsWithTag("Button");
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].GetComponent<ButtonColourController>().ReloadButton();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!bGameOver)
        {
            //if (Input.GetKeyDown(KeyCode.P))
            //    GameOver();
            EscapeClicked();
            if (bPlay)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bPlay = false;
                SearchAllButtons();
                Time.timeScale = 1.0f;
                SceneManager.LoadScene(1);
            }
            if (bHowToPlay)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bHowToPlay = false;
                mainMenu_PauseMenuCanvas.SetActive(false);
                howToPlayCanvas.SetActive(true);
                SearchAllButtons();
            }
            if (bSettings)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bSettings = false;
                mainMenu_PauseMenuCanvas.SetActive(false);
                settingsCanvas.SetActive(true);
                SearchAllButtons();
                if (SceneManager.GetActiveScene().buildIndex != 0)
                {
                    bSettingsPaused = true;
                }
            }
            if (bBack)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bBack = false;
                bSettingsPaused = false;
                mainMenu_PauseMenuCanvas.SetActive(true);
                settingsCanvas.SetActive(false);
                SearchAllButtons();
                if (howToPlayCanvas != null)
                {
                    howToPlayCanvas.SetActive(false);
                }
                if (CreditsCanvas != null)
                {
                    CreditsCanvas.SetActive(false);
                }
            }
            if (bResume)
            {
                SearchAllButtons();
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bPaused = false;
                bResume = false;
                mainMenu_PauseMenuCanvas.SetActive(false);
                Time.timeScale = 1.0f;
            }
            if (bMainMenu)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bMainMenu = false;
                SearchAllButtons();
                SceneManager.LoadScene(0);
            }
            if (bPaused)
            {

                if (!bSettingsPaused)
                {
                    mainMenu_PauseMenuCanvas.SetActive(true);
                    Time.timeScale = 0.0f;
                }
            }
            else if (!bPaused && SceneManager.GetActiveScene().buildIndex != 0)
            {
                SearchAllButtons();
                bSettingsPaused = false;
                mainMenu_PauseMenuCanvas.SetActive(false);
                settingsCanvas.SetActive(false);
                Time.timeScale = 1.0f;
            }
            if (bCredits)
            {
                SearchAllButtons();
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bCredits = false;
                mainMenu_PauseMenuCanvas.SetActive(false);
                CreditsCanvas.SetActive(true);

            }
            if (bExit)
            {
                SearchAllButtons();
                Debug.Log("Application Quit");
                bExit = false;
                Application.Quit();
            }
        }
        else
        {

            Time.timeScale = 0.0f;

            if (bExit)
            {
                SearchAllButtons();
                Debug.Log("Application Quit");
                bExit = false;
                Application.Quit();
            }
            if (bMainMenu)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bMainMenu = false;
                SearchAllButtons();
                SceneManager.LoadScene(0);
            }
            if (bPlay)
            {
                if (SC != null)
                    SC.GetComponent<SoundController>().PlayButtonClickedSound(buttonClickedSound);
                bPlay = false;
                SearchAllButtons();
                Time.timeScale = 1.0f;
                SceneManager.LoadScene(1);
            }
        }
    }

    public void PlayClicked()
    {
        bPlay = true;

    }

    public void HowToPlayClicked()
    {
        bHowToPlay = true;
    }

    public void SettingsClicked()
    {
        bSettings = true;
    }

    public void ExitClicked()
    {
        bExit = true;
    }

    public void BackClicked()
    {
        bBack = true;


    }
    public void ResumeClicked()
    {
        bResume = true;
    }
    public void MainMenuClicked()
    {
        bMainMenu = true;
    }
    public void PausedClicked()
    {
        bPaused = true;
    }
    public void CreditsClicked()
    {
        bCredits = true;
    }

    void EscapeClicked()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            bPaused = !bPaused;
        }
    }

    public void MusicVolumeChanged()
    {
        float fMusicVol;
        fMusicVol = musicVol.value;
        if (SC != null)
            SC.GetComponent<SoundController>().MusicVolumeChange(fMusicVol);
        string sPersent;
        sPersent = (fMusicVol * 100).ToString("F1") + "%";
        musicVolumePercent.text = sPersent;
    }
    public void SoundFXVolumeChanged()
    {
        float fSFXVol;
        fSFXVol = soundFXVol.value;
        if (SC != null)
            SC.GetComponent<SoundController>().SoundFXVolumeChange(fSFXVol);
        string sPersent;
        sPersent = (fSFXVol * 100).ToString("F1") + "%";
        soundFXVolumePercent.text = sPersent;
    }
    //turtal dog bird snake
    public void GameOver(bool bDog, bool bSnake, bool bBird, bool bTurtal)
    {
        bGameOver = true;
        Time.timeScale = 0.0f;
        GameOverCanvas.SetActive(true);
        if (bTurtal)
        {
            Animal1.sprite = turtalSprite;
            bTurtal = false;
        }
        else if (bDog)
        {
            Animal1.sprite = dogSprite;
            bDog = false;
        }
        else if (bBird)
        {
            Animal1.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal1.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal1.sprite = null;
            var tempColor = Animal1.color;
            tempColor.a = 0.0f;
            Animal1.color = tempColor;
        }

        if (bDog)
        {
            Animal2.sprite = dogSprite;
            bDog = false;
        }
        else if (bBird)
        {
            Animal2.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal2.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal2.sprite = null;
            var tempColor = Animal2.color;
            tempColor.a = 0.0f;
            Animal2.color = tempColor;
        }

        if (bBird)
        {
            Animal3.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal3.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal3.sprite = null;
            var tempColor = Animal3.color;
            tempColor.a = 0.0f;
            Animal3.color = tempColor;
        }

        if (bSnake)
        {
            Animal4.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal4.sprite = null;
            var tempColor = Animal4.color;
            tempColor.a = 0.0f;
            Animal4.color = tempColor;
        }
    }

    public void GameWon(bool bDog, bool bSnake, bool bBird, bool bTurtal)
    {
        bGameOver = true;
        Time.timeScale = 0.0f;
        GameWonCanvas.SetActive(true);
        if (bTurtal)
        {
            Animal1Won.sprite = turtalSprite;
            bTurtal = false;
        }
        else if (bDog)
        {
            Animal1Won.sprite = dogSprite;
            bDog = false;
        }
        else if (bBird)
        {
            Animal1Won.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal1Won.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal1Won.sprite = null;
            var tempColor = Animal1Won.color;
            tempColor.a = 0.0f;
            Animal1Won.color = tempColor;
        }

        if (bDog)
        {
            Animal2Won.sprite = dogSprite;
            bDog = false;
        }
        else if (bBird)
        {
            Animal2Won.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal2Won.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal2Won.sprite = null;
            var tempColor = Animal2Won.color;
            tempColor.a = 0.0f;
            Animal2Won.color = tempColor;
        }

        if (bBird)
        {
            Animal3Won.sprite = birdSprite;
            bBird = false;
        }
        else if (bSnake)
        {
            Animal3Won.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal3Won.sprite = null;
            var tempColor = Animal3Won.color;
            tempColor.a = 0.0f;
            Animal3Won.color = tempColor;
        }

        if (bSnake)
        {
            Animal4Won.sprite = snakeSprite;
            bSnake = false;
        }
        else
        {
            Animal4Won.sprite = null;
            var tempColor = Animal4Won.color;
            tempColor.a = 0.0f;
            Animal4Won.color = tempColor;
        }
    }
}