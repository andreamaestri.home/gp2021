﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    PlayerController playerController;
    GameManager gameManager;

    private const float maxTrigStayTimer = 0.1f;
    private float trigStayTimer = maxTrigStayTimer;

    private bool otherSide = false; public void setOtherSide(bool b) { otherSide = b; } public bool isOtherSide() { return otherSide; } 

    // Start is called before the first frame update
    void Start()
    {
        playerController = transform.parent.GetComponent<PlayerController>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (otherSide)
        {
            trigStayTimer -= Time.deltaTime;

            if (trigStayTimer <= 0f)
            {
                playerController.setCanSwap(true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (otherSide)
        {
            playerController.setCanSwap(false);
            trigStayTimer = maxTrigStayTimer;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (otherSide)
        {
            playerController.setCanSwap(false);
            trigStayTimer = maxTrigStayTimer;
        }
    }

    private void OnTriggerExit(Collider other)
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == GameData.Boundary)
            gameManager.endGame(false);
    }

    private void OnCollisionStay(Collision collision)
    {
        //Debug.Log("stay");
    }

    private void OnCollisionExit(Collision collision)
    {
        //Debug.Log("exit");
    }
}
