﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetController : MonoBehaviour
{
    private bool active = false; public bool isActive() { return active; }
    private float activeAfterPositioned = 2f;

    GameManager gameManager;
    PlayerController playerController;

    GameObject topSprite, mirrorSprite;
    SpriteRenderer topSpriteRenderer, mirrorSpriteRenderer;

    private Vector2 topFollowPosition; public void setTopFollowPoisition(float fpX, float fpY) { topFollowPosition.x = fpX; topFollowPosition.y = fpY; } public Vector2 getTopFollowPosition() { return topFollowPosition; }
    private int followPositionIndex = 0; public void setFollowPoisitionIndex(int fp) { followPositionIndex = fp; } public int getFollowPositionIndex() { return followPositionIndex; }

    private PetController petFollowing; public void setPetFollowing(PetController p) { petFollowing = p; }

    //Make pets run all the time in the background with their default Y and changing X values and their sprite renderers turned off

    public void awakenPet()
    {
        setPetColours(playerController.getTopColor(), playerController.getMirrorColor());
        activeAfterPositioned = 0.1f;
    }

    public void resetPet()
    {
        topSpriteRenderer.enabled = false;
        mirrorSpriteRenderer.enabled = false;
        active = false;
    }

    public void setPetColours(Color topCol, Color mirrorCol)
    {
        topSpriteRenderer.color = topCol;
        mirrorSpriteRenderer.color = mirrorCol;
    }

    public void setupPet()
    {
        topFollowPosition = new Vector2();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        topSprite = transform.Find("Top").gameObject;
        mirrorSprite = transform.Find("Mirror").gameObject;
        topSpriteRenderer = topSprite.GetComponent<SpriteRenderer>();
        mirrorSpriteRenderer = mirrorSprite.GetComponent<SpriteRenderer>();
        topSpriteRenderer.enabled = false;
        mirrorSpriteRenderer.enabled = false;
        active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf && Time.timeScale != 0f)
        {
            if (petFollowing != null && petFollowing.gameObject.activeSelf)
                petFollowing.setTopFollowPoisition(topSprite.transform.position.x - GameData.petSpacing, topSprite.transform.position.y);

            //Set Y value to minimum fi this isn't active yet
            if (!isActive())
                topSprite.transform.position = new Vector3(topFollowPosition.x, GameData.getLowestValueY(), 0f);
            else//Move top sprite on X and Y since it's active
                topSprite.transform.Translate(topFollowPosition.x - topSprite.transform.position.x, (topFollowPosition.y - topSprite.transform.position.y) * 0.1f, 0f);
            //Mirror the Y axis and copy the position to the mirror image
            mirrorSprite.transform.position = new Vector3(topSprite.transform.position.x, -topSprite.transform.position.y, 0f);

            if (activeAfterPositioned <= 1f && activeAfterPositioned > 0f)
                activeAfterPositioned -= Time.deltaTime;
            else if (activeAfterPositioned <= 0f)
            {
                activeAfterPositioned = 2f;
                topSpriteRenderer.enabled = true;
                mirrorSpriteRenderer.enabled = true;
                active = true;
            }

        }
    }
}
