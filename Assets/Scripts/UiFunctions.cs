﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiFunctions : MonoBehaviour
{
    public void PauseGame()
    {
        Time.timeScale = 1f;
    }

    public void UnPauseGame()
    {
        Time.timeScale = 0f;
    }
}
