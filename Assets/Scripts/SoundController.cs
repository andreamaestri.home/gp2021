﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public static SoundController SC = null;

    public AudioSource buttonClickedSource;
    public AudioSource mainMusicSource;
    public AudioSource soundFXSource;

    float fOldMusicVolume = 0.5f;
    public float GetOldMusicVolume()
    {
        return fOldMusicVolume;
    }
    public void SetOldMusicVolume(float fNewVolume)
    {
        fOldMusicVolume = fNewVolume;
    }

    float fOldSFXVolume = 0.5f;
    public float GetOldSFXVolume()
    {
        return fOldSFXVolume;
    }
    public void SetOldSFXVolume(float fNewVolume)
    {
        fOldSFXVolume = fNewVolume;
    }

    
    // Start is called before the first frame update
    void Start()
    {
        if (SC == null)
        {
            SC = this;
            DontDestroyOnLoad(transform.gameObject);
            if(mainMusicSource.clip != null)
                mainMusicSource.Play();
        }
        else
        {
            Destroy(this);
        }
    }

    public void PlayButtonClickedSound(AudioClip buttonClicked)
    {
        buttonClickedSource.clip = buttonClicked;
        buttonClickedSource.Play();
    }

    public void PlaySFXSound(AudioClip soundFX)
    {
        soundFXSource.clip = soundFX;
        soundFXSource.Play();
    }

    public void MusicVolumeChange(float newVolume)
    {
        fOldMusicVolume = newVolume;
        mainMusicSource.volume = newVolume;
    }

    public void SoundFXVolumeChange(float newVolume)
    {
        fOldSFXVolume = newVolume;
        soundFXSource.volume = newVolume;
        buttonClickedSource.volume = newVolume;
    }
}
