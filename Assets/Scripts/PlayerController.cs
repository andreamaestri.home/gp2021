﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private GameManager gameManager;
    private GameObject playerTop, playerMirror; public GameObject getPlayerTop() { return playerTop; }
    private Rigidbody rigidbodyTop, rigidbodyMirror;
    private SpriteRenderer spriteRendererTop, spriteRendererMirror;
    private SphereCollider circleColliderTop, circleColliderMirror;
    private PlayerCollider playerColliderTop, playerColliderMirror;
    private Transform feetTop, feetMirror;
    public Color getTopColor() { return spriteRendererTop.color; }
    public Color getMirrorColor() { return spriteRendererMirror.color; }

    private Animator invulerableAnimator;
    public void setInvulnerablity(bool b)
    {
        invulerableAnimator.SetBool("invulnerable", b);
    }

    private Vector3 feetBox;
    private Vector3 swapBox;

    private Vector3 moveDir;//For moving active side
    private Vector3 copyPos;//For copying position to other side
    private Vector3 fixPos;//For fixing the position of the active side

    private float jumpModifier = 100f;//200f;
    private float runModifier = 100f;
    private float gravityModifier = 98f;

    //Tells the game which half of the player is active
    private bool notMirrored = true;
    //Tells the player if they need to change next time they touch the ground 
    private bool newSide = true;

    private bool canSwap = false; public void setCanSwap(bool b) { canSwap = b; }

    private const float maxSwapTimer = 0.1f;//Half of max value as it will run from minus this value
    private float swapTimer = maxSwapTimer;

    private const float maxJumpTimer = 0.5f;
    private float jumpTimer = 0f;

    private const uint maxHP = 3;
    private uint HP; public void addHP(uint h) { if (HP + h <= maxHP) HP += h; }
    public void damage(uint d)
    {
        if (!isInvincible())
        {
            if (HP - d < 0) HP = 0;
            else HP -= d;
            invincibleTimer = maxInvincibleTime;//Set player to be invincible
        }
    }

    private const float maxInvincibleTime = 3f;
    private float invincibleTimer = 0f; public bool isInvincible() { if (invincibleTimer > 0f) return true; return false; }

    void Awake()
    {
        moveDir = new Vector3(0f, 0f, 0f);
        copyPos = new Vector3(0f, 0f, 0f);
        fixPos = new Vector3(0f, 0f, 0f);
        feetBox = new Vector3(0.08f, 0.01f, 0.001f);
        swapBox = new Vector3(0.07f, 0.07f, 0.001f);
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        //Retrieve child objects for the player
        playerTop = gameObject.transform.Find("PlayerTop").gameObject;
        playerMirror = gameObject.transform.Find("PlayerMirror").gameObject;

        rigidbodyTop = playerTop.GetComponent<Rigidbody>();
        rigidbodyMirror = playerMirror.GetComponent<Rigidbody>();
        spriteRendererTop = playerTop.GetComponent<SpriteRenderer>();
        spriteRendererMirror = playerMirror.GetComponent<SpriteRenderer>();
        circleColliderTop = playerTop.GetComponent<SphereCollider>();
        circleColliderMirror = playerMirror.GetComponent<SphereCollider>();
        playerColliderTop = playerTop.GetComponent<PlayerCollider>();
        playerColliderMirror = playerMirror.GetComponent<PlayerCollider>();
        feetTop = playerTop.transform.Find("Feet");
        feetMirror = playerMirror.transform.Find("Feet");

        invulerableAnimator = GetComponent<Animator>();

        HP = maxHP;
    }

    private void Start()
    {
        //Setup for game start
        HP = maxHP;
        spriteRendererTop.color = gameManager.getAssetManager().getColourTop();
        spriteRendererMirror.color = gameManager.getAssetManager().getColourMirrorDisabled();
        circleColliderTop.enabled = true;
        circleColliderMirror.enabled = false;
        playerTop.layer = GameData.PlayerActive;
        playerMirror.layer = GameData.PlayerInactive;
        playerColliderTop.setOtherSide(false);
        playerColliderMirror.setOtherSide(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (HP == 0)
            gameManager.endGame(false);

        if (invincibleTimer >= 0f)
            invincibleTimer -= Time.deltaTime;

        if (jumpTimer < maxJumpTimer)
            jumpTimer += Time.deltaTime;

        moveDir.y = 0f;


        /*if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            moveDir.x = -1f;
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            moveDir.x = 1f;
        else
            moveDir.x = 0f;*/

        if ((newSide && swapTimer == maxSwapTimer) || (!newSide && swapTimer == -maxSwapTimer))//Only allow jumping or swap if 
        {
            if (jumpTimer >= maxJumpTimer && (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && isOnGround(""))
            {
                                 
                GameObject.Find("SoundController").GetComponent<SoundController>().PlaySFXSound(gameManager.getAssetManager().getSoundClip_jump());
                jumpTimer = 0f;
                moveDir.y = (notMirrored) ? 1f : -1f;
            }
            //else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            //    moveDir.y = (notMirrored) ? -1f : 1f;
        }
        else
        {
            //Update swaping timer
            swapTimer += (newSide) ? Time.deltaTime : -Time.deltaTime;
            if ((newSide && swapTimer >= maxSwapTimer) || (!newSide && swapTimer <= -maxSwapTimer))
            {//Swapping is complete, make sure values are right to run the player
                swapTimer = (newSide) ? maxSwapTimer : -maxSwapTimer;
                notMirrored = newSide;
                if (notMirrored)
                {
                    spriteRendererTop.color = gameManager.getAssetManager().getColourTop();
                    spriteRendererMirror.color = gameManager.getAssetManager().getColourMirrorDisabled();
                    gameManager.updatePetMirroring(gameManager.getAssetManager().getColourTop(), gameManager.getAssetManager().getColourMirrorDisabled());
                    circleColliderTop.enabled = true;
                    circleColliderMirror.enabled = false;
                    playerTop.layer = GameData.PlayerActive;
                    playerMirror.layer = GameData.PlayerInactive;
                    playerColliderTop.setOtherSide(false);
                    playerColliderMirror.setOtherSide(true);
                }
                else
                {
                    spriteRendererMirror.color = gameManager.getAssetManager().getColourMirror();
                    spriteRendererTop.color = gameManager.getAssetManager().getColourTopDisabled();
                    gameManager.updatePetMirroring(gameManager.getAssetManager().getColourTopDisabled(), gameManager.getAssetManager().getColourMirror());
                    circleColliderTop.enabled = false;
                    circleColliderMirror.enabled = true;
                    playerTop.layer = GameData.PlayerInactive;
                    playerMirror.layer = GameData.PlayerActive;
                    playerColliderTop.setOtherSide(true);
                    playerColliderMirror.setOtherSide(false);
                }
            }
            else
            {//Swapping is still in progress

            }
        }

        //Allows player to change side whilst changing is in process
        if (Input.GetKeyDown(KeyCode.Space) && notMirrored == newSide && isOnGround("") && isAbleToSwap())//isOnGround(GameData.Tag_Floor) && isAbleToSwap())
        {
            newSide = !newSide;//Change side next time you touch the ground
        }

        //Update player's physics
        if (notMirrored)
            updatePlayerPhysics(rigidbodyTop, playerMirror.transform);
        else
            updatePlayerPhysics(rigidbodyMirror, playerTop.transform);
    }

    private void updatePlayerPhysics(Rigidbody activePlayer, Transform followingPlayer)
    {
        //Move player back to the centre from the left
        if (activePlayer.transform.localPosition.x < 0f)
            activePlayer.AddForce(Vector3.right * 20f * Time.deltaTime);

        if (moveDir.y != 0)
        {
            //Minus current velocity from jumpModifier because the value of jumpModifier is the max Y velocity there should be
            moveDir.y *= ((notMirrored) ? jumpModifier - activePlayer.velocity.y : jumpModifier + activePlayer.velocity.y);//-((-jumpModifier) - activePlayer.velocity.y)
            activePlayer.AddForce(Vector3.up * moveDir.y);
            //Debug.Log("Y velocity: " + (Vector3.up * moveDir.y));
        }

        //Debug.Log("player Y velocity: " + activePlayer.velocity.y);

        //Add Force to simulate gravity
        activePlayer.AddForce(((notMirrored) ? Vector3.down : Vector3.up) * Time.deltaTime * gravityModifier);

        if ((notMirrored && activePlayer.transform.localPosition.y < GameData.getLowestValueY()) || (!notMirrored && activePlayer.transform.localPosition.y > -GameData.getLowestValueY()))
        {
            fixPos.x = activePlayer.transform.localPosition.x;
            fixPos.y = GameData.getLowestValueY() * ((notMirrored)? 1f : -1f);
            activePlayer.transform.localPosition = fixPos;
        }
        copyPos.x = activePlayer.transform.localPosition.x;
        copyPos.y = -activePlayer.transform.localPosition.y;
        followingPlayer.transform.localPosition = copyPos;//Might be worth moving this line if it's doing pass by reference
    }

    private bool isOnGround(string layerType)
    {
        //Test for overlaps with other hit boxes
        Collider[] colliders = Physics.OverlapBox((notMirrored? feetTop : feetMirror).position, feetBox);
        //Search for the one collider which is an obstalce of the florr, which can be jumped on
        foreach (Collider col in colliders)
        {
            if (layerType == "" && (col.tag == GameData.Tag_Obstacle || col.tag == GameData.Tag_Floor))
                return true;
            else if (col.tag == layerType)
                return true;
        }
        return false;
    }

    private bool isAbleToSwap()
    {
        //Test for overlaps with other hit boxes
        Collider[] colliders = Physics.OverlapBox((notMirrored ? playerMirror : playerTop).transform.position, swapBox);
        //Search for the one collider which is an obstalce of the florr, which can be jumped on
        foreach (Collider col in colliders)
        {
            if (col.tag == GameData.Tag_Obstacle)
                return false;
        }
        return true;
    }
}
