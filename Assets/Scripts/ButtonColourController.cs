﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonColourController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public Text tText;

    void start()
    {
        //tText = this.transform.GetChild(0).gameObject.GetComponent<Text>();
    }
    public void ReloadButton()
    {
        tText.color = Color.white;
    }
    // Start is called before the first frame update
    public void OnPointerEnter(PointerEventData eventData)
    {
        tText.color = Color.black;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tText.color = Color.white;
    }
}
