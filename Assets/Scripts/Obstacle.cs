﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private GameManager gameManager;

    private Vector3 constVelocity;
    private float speed = -1f;

    private Rigidbody rigidbody;
    private SpriteRenderer spriteRenderer;

    public void setupObstacle(Color colour)
    {
        if (!spriteRenderer)
            firstSetup();
        spriteRenderer.color = colour;
    }

    private void firstSetup()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        rigidbody = GetComponent<Rigidbody>();
        constVelocity = new Vector3(speed, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localPosition.x < gameManager.getRemovePoint().localPosition.x && gameObject.activeSelf)
            removeSelf();
        else
        {
            //if (newPos.y != transform.localPosition.y)
            //    newPos.y = transform.localPosition.y;
            //newPos.x = transform.localPosition.x - (speed * Time.deltaTime);
            //transform.localPosition = newPos;
            rigidbody.velocity = constVelocity;
        }
    }

    //Remove this from the level and put it back into the asset pool
    private void removeSelf()
    {
        //if (gameObject.tag == GameData.Tag_PetCollect_Bird || gameObject.tag == GameData.Tag_PetCollect_Turtle || gameObject.tag == GameData.Tag_PetCollect_Snake || gameObject.tag == GameData.Tag_PetCollect_Dog)
        //    gameManager.setPetInGame(false);//Set pet in game to be false so another one can be spawned
        //else 

        //Call GameManager to increment the score, but not for missed pets
        if (!(gameObject.tag == GameData.Tag_PetCollect_Bird || gameObject.tag == GameData.Tag_PetCollect_Turtle || gameObject.tag == GameData.Tag_PetCollect_Snake || gameObject.tag == GameData.Tag_PetCollect_Dog))
            gameManager.addToScore(1);
        else
        {
            switch (gameObject.tag)
            {
                case GameData.Tag_PetCollect_Bird:
                    gameManager.setPetTypeDespawned((int)GameData.PetIndex.Bird, true);
                    break;
                case GameData.Tag_PetCollect_Turtle:
                    gameManager.setPetTypeDespawned((int)GameData.PetIndex.Turtle, true);
                    break;
                case GameData.Tag_PetCollect_Snake:
                    gameManager.setPetTypeDespawned((int)GameData.PetIndex.Snake, true);
                    break;
                case GameData.Tag_PetCollect_Dog:
                    gameManager.setPetTypeDespawned((int)GameData.PetIndex.Dog, true);
                    break;
                default:
                    break;
            }
        }

        //Make self inactive so the GameManager can use  this again from the object pool
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == GameData.Tag_PlayerTop || other.gameObject.tag == GameData.Tag_PlayerMirror)
        {
            /*if (gameObject.tag == GameData.Tag_Obstacle)
                gameManager.getPlayerController().damage(1);
            else if (gameObject.tag == GameData.Tag_PowerUp_Invincible)
                gameManager.getPlayerController().damage(1);
            else if (gameObject.tag == GameData.Tag_PowerUp_AddHealth)
                gameManager.getPlayerController().addHP(1);*/
            switch (gameObject.tag)
            {
                case GameData.Tag_PetCollect_Bird:
                    gameManager.addPet((int)GameData.PetIndex.Bird);
                    removeSelf();
                    break;
                case GameData.Tag_PetCollect_Turtle:
                    gameManager.addPet((int)GameData.PetIndex.Turtle);
                    removeSelf();
                    break;
                case GameData.Tag_PetCollect_Snake:
                    gameManager.addPet((int)GameData.PetIndex.Snake);
                    removeSelf();
                    break;
                case GameData.Tag_PetCollect_Dog:
                    gameManager.addPet((int)GameData.PetIndex.Dog);
                    removeSelf();
                    break;
                case GameData.Tag_Conker:
                    gameManager.hitConker();
                    break;
                default:
                    break;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        
    }
}
