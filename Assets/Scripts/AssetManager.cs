﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager : MonoBehaviour
{
    public Color colourTop, colourMirror;
    private Color colourTopDisabled, colourMirrorDisabled;
    public float disabledAlpha;
    public Color getColourTop() { return colourTop; } public Color getColourMirror() { return colourMirror; }
    public Color getColourTopDisabled() { return colourTopDisabled; } public Color getColourMirrorDisabled() { return colourMirrorDisabled; }

    private static Vector2 setVector2(float x, float y) { return new Vector2(x, y); }

    private static Vector2 halfDimensionsObstacle = setVector2(0.08f, 0.08f); public Vector2 getHalfDimensionsObstacle() { return halfDimensionsObstacle; }

    public GameObject obstacle_wall0; private GameObject[] obstacle_wall0s; public GameObject[] getObstacle_wall0s() { return obstacle_wall0s; }

    public GameObject obstacle_wall1; private GameObject[] obstacle_wall1s; public GameObject[] getObstacle_wall1s() { return obstacle_wall1s; }

    public GameObject obstacle_trunk0; private GameObject[] obstacle_trunk0s; public GameObject[] getObstacle_trunk0s() { return obstacle_trunk0s; }

    public GameObject obstacle_trunk1; private GameObject[] obstacle_trunk1s; public GameObject[] getObstacle_trunk1s() { return obstacle_trunk1s; }

    public GameObject obstacle_conker; private GameObject[] obstacle_conkers; public GameObject[] getObstacle_conkers() { return obstacle_conkers; }

    public GameObject obstacle_platform0; private GameObject[] obstacle_platform0s; public GameObject[] getObstacle_platform0s() { return obstacle_platform0s; }
    public GameObject obstacle_platform1; private GameObject[] obstacle_platform1s; public GameObject[] getObstacle_platform1s() { return obstacle_platform1s; }
    public GameObject obstacle_platform2; private GameObject[] obstacle_platform2s; public GameObject[] getObstacle_platform2s() { return obstacle_platform2s; }

    public AudioClip soundClip_jump; public AudioClip getSoundClip_jump() { return soundClip_jump; }

    public GameObject obstacle_ee; private GameObject[] obstacle_eemask; public GameObject getObstacle_eemask() { return obstacle_eemask[0]; }

    public GameObject pet_bird; private GameObject[] pet_birds; public GameObject getPet_bird() { return pet_birds[0]; }
    public GameObject pet_turtle; private GameObject[] pet_turtles; public GameObject getPet_turtle() { return pet_turtles[0]; }
    public GameObject pet_snake; private GameObject[] pet_snakes; public GameObject getPet_snake() { return pet_snakes[0]; }
    public GameObject pet_dog; private GameObject[] pet_dogs; public GameObject getPet_dog() { return pet_dogs[0]; }

    void Awake()
    {
        setupObjectPool(obstacle_wall0s = new GameObject[10], obstacle_wall0);
        setupObjectPool(obstacle_wall1s = new GameObject[10], obstacle_wall1);
        setupObjectPool(obstacle_trunk0s = new GameObject[10], obstacle_trunk0);
        setupObjectPool(obstacle_trunk1s = new GameObject[10], obstacle_trunk1);
        setupObjectPool(obstacle_conkers = new GameObject[10], obstacle_conker);
        setupObjectPool(obstacle_platform0s = new GameObject[5], obstacle_platform0);
        setupObjectPool(obstacle_platform1s = new GameObject[10], obstacle_platform1);
        setupObjectPool(obstacle_platform2s = new GameObject[5], obstacle_platform2);
        setupObjectPool(obstacle_eemask = new GameObject[1], obstacle_ee);

        setupObjectPool(pet_birds = new GameObject[1], pet_bird);
        setupObjectPool(pet_turtles = new GameObject[1], pet_turtle);
        setupObjectPool(pet_snakes = new GameObject[1], pet_snake);
        setupObjectPool(pet_dogs = new GameObject[1], pet_dog);

        colourTopDisabled = new Color(colourTop.r, colourTop.g, colourTop.b, disabledAlpha);
        colourMirrorDisabled = new Color(colourMirror.r, colourMirror.g, colourMirror.b, disabledAlpha);
    }

    private void setupObjectPool(GameObject[] arr, GameObject prefab)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = Instantiate(prefab);
            arr[i].SetActive(false);//Will be reactivated when it is used
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
