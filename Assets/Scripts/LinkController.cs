﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LinkController : MonoBehaviour
{
    public string URL;

    public void OpenWebPage()
    {
        Application.OpenURL(URL);
    }
}
