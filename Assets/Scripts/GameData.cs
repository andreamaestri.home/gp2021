﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    //Game Data
    private static float lowestValueY = 0.08008f; public static float getLowestValueY() { return lowestValueY; }

    //Physics Layer Collision Index
    public const int PlayerActive = 8;
    public const int PlayerInactive = 9;
    public const int Obstacle = 10;
    public const int Boundary = 11;
    public const int Floor = 12;

    //Tags
    public const string Tag_PlayerTop = "PlayerTop";
    public const string Tag_PlayerMirror = "PlayerMirror";
    public const string Tag_Floor = "Floor";
    public const string Tag_Obstacle = "Obstacle";
    public const string Tag_PowerUp_Invincible = "PowerUp_Invincible";
    public const string Tag_PowerUp_AddHealth = "PowerUp_AddHealth";
    public const string Tag_PetCollect_Bird = "PetCollect_Bird";
    public const string Tag_PetCollect_Dog = "PetCollect_Dog";
    public const string Tag_PetCollect_Snake = "PetCollect_Snake";
    public const string Tag_PetCollect_Turtle = "PetCollect_Turtle";
    public const string Tag_Conker = "Conker";

    public const float petSpacing = 0.3f;
    public enum PetIndex { Bird, Turtle, Snake, Dog };

    public static int getHighScore()
    {
        return PlayerPrefs.GetInt("HighScore", 0);
    }

    public static bool testHighScore(int newScore)
    {
        if (newScore > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", newScore);
            return true;
        }
        return false;
    }
}
